﻿using Game;
using UnityEngine;

namespace Factory
{
    public class GameFactory
    {
        public static void SpawnSecondPlayer()
        {
            PhotonNetwork.Instantiate("Kyle", GetPlayerPosition(), Quaternion.identity, 0);
        }
        
        public static Vector3 GetPlayerPosition()
        {
            return new Vector3(0f, 0.6f, -20f);
        }
    }
}