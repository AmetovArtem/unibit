﻿using UnityEngine;
using System.Collections;

namespace Game.Control
{
    [RequireComponent(typeof(CharacterController))]
    public class PlayerController : Photon.MonoBehaviour
    {

        private CharacterController _characterController;
        private Vector3 _lastPosition;
        private Animator _animator;
        private PhotonView _photonView;
        private PhotonTransformView _transformView;
        private float _animatorSpeed;
        private Vector3 _currentMovement;
        private float _currentRotation;
        private float _speedModifier;

        [SerializeField] private float DirectionDampTime = 0.25f;
        [SerializeField] private float TurnSpeedModifier = 0.5f;
        [SerializeField] private float SynchronizedMaxSpeed = 6f;
        [SerializeField] private float SynchronizedTurnSpeed = 120f;

        private void Start()
        {
            _characterController = GetComponent<CharacterController>();
            _animator = GetComponent<Animator>();
            _photonView = GetComponent<PhotonView>();
            _transformView = GetComponent<PhotonTransformView>();
        }
        
        private void Update()
        {
            UpdateMovement();
            return;
        }

        private void UpdateMovement()
        {
            if (_animator)
            {
                AnimatorStateInfo stateInfo = _animator.GetCurrentAnimatorStateInfo(0);

                if (stateInfo.IsName("Base Layer.Run"))
                {
                    if (Input.GetButton("Fire1"))
                        _animator.SetBool("Jump", true);
                }
                else
                {
                    _animator.SetBool("Jump", false);
                }

                if (Input.GetButtonDown("Fire2") && _animator.layerCount >= 2)
                {
                    _animator.SetBool("Hi", !_animator.GetBool("Hi"));
                }


                float Horizontal = Input.GetAxis("Horizontal");
                float Vertical = Input.GetAxis("Vertical");

                if (Vertical < 0)
                {
                    Vertical = 0;
                }

                _animator.SetFloat("Speed", Horizontal * Horizontal + Vertical * Vertical);
                _animator.SetFloat("Direction", Horizontal, DirectionDampTime, Time.deltaTime);

                float direction = _animator.GetFloat("Direction");

                float targetSpeedModifier = Mathf.Abs(Vertical);

                if (Mathf.Abs(direction) > 0.2f)
                {
                    targetSpeedModifier = TurnSpeedModifier;
                }

                _speedModifier = Mathf.MoveTowards(_speedModifier, targetSpeedModifier, Time.deltaTime * 25f);

                Vector3 speed = transform.forward * SynchronizedMaxSpeed * _speedModifier;
                float turnSpeed = direction * SynchronizedTurnSpeed;

                _transformView.SetSynchronizedValues(speed, turnSpeed);
            }
        }

        private void UpdateAnimation()
        {
            Vector3 _movementVector = transform.position - _lastPosition;

            float _speed = Vector3.Dot(_movementVector.normalized, transform.forward);
            float _direction = Vector3.Dot(_movementVector.normalized, transform.right);

            if (Mathf.Abs(_speed) < 0.2f)
            {
                _speed = 0f;
            }

            if (_speed > 0.6f)
            {
                _speed = 1f;
                _direction = 0f;
            }

            if (_speed >= 0f)
            {
                if (Mathf.Abs(_direction) > 0.7f)
                {
                    _speed = 1f;
                }
            }

            _animatorSpeed = Mathf.MoveTowards(_animatorSpeed, _speed, Time.deltaTime * 5f);

            _animator.SetFloat("Speed", _animatorSpeed);
            _animator.SetFloat("Direction", _direction);

            _lastPosition = transform.position;
        }
        
        protected void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            //if (stream.isReading)
            //{
            //    _currentMovement = (Vector3)stream.ReceiveNext();
            //    _currentRotation = (float)stream.ReceiveNext();
            //    //_currentTurnSpeed = (Vector3)stream.ReceiveNext();
            //}

            if (stream.isWriting)
            {
                Debug.Log("M: " + _currentMovement);
                Debug.Log("R: " + _currentRotation);
                stream.SendNext(_currentMovement);
                stream.SendNext(_currentRotation);
            }
        }
    }
}