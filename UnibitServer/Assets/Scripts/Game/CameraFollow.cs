﻿using UnityEngine;
using System.Collections;

namespace Game
{
    public class CameraFollow : MonoBehaviour
    {
        [SerializeField] private Transform Target;

        [SerializeField] private float MaximumDistance;
        [SerializeField] private float MinimumDistance;

        [SerializeField] private float ScrollModifier;
        [SerializeField] private float TurnModifier;

        private Transform _cameraTransform;

        private Vector3 _lookAtPoint;
        private Vector3 _localForwardVector;
        private float _distance;

        private void Start()
        {
            _cameraTransform = transform.GetChild(0);
            _localForwardVector = _cameraTransform.forward;

            _distance = -_cameraTransform.localPosition.z / _cameraTransform.forward.z;
            _distance = Mathf.Clamp(_distance, MinimumDistance, MaximumDistance);
            _lookAtPoint = _cameraTransform.localPosition + _localForwardVector * _distance;
        }

        private void LateUpdate()
        {
            UpdateDistance();
            UpdateZoom();
            UpdatePosition();
            UpdateRotation();
        }

        private void UpdateDistance()
        {
            _distance = Mathf.Clamp(_distance - Input.GetAxis("Mouse ScrollWheel") * ScrollModifier, MinimumDistance, MaximumDistance);
        }

        private void UpdateZoom()
        {
            _cameraTransform.localPosition = _lookAtPoint - _localForwardVector * _distance;
        }

        private void UpdatePosition()
        {
            if (Target == null)
            {
                return;
            }

            transform.position = Target.transform.position;
        }

        private void UpdateRotation()
        {
            if (Input.GetMouseButton(0) == true || Input.GetMouseButton(1) == true || Input.GetButton("Fire1") || Input.GetButton("Fire2"))
            {
                transform.Rotate(0, Input.GetAxis("Mouse X") * TurnModifier, 0);
            }

            if ((Input.GetMouseButton(1) || Input.GetButton("Fire2")) && Target != null)
            {
                Target.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
            }
        }
    }
}