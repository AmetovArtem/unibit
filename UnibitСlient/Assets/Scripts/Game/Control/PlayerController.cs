﻿using UnityEngine;
using System.Collections;

namespace Game.Control
{
    [RequireComponent(typeof(CharacterController))]
    public class PlayerController : Photon.MonoBehaviour
    {
        private CharacterController _characterController;
        private Animator _animator;
        private PhotonView _photonView;
        private PhotonTransformView _transformView;
        
        private void Start()
        {
            _characterController = GetComponent<CharacterController>();
            _animator = GetComponent<Animator>();
            _photonView = GetComponent<PhotonView>();
            _transformView = GetComponent<PhotonTransformView>();
        }
    }
}